package com.psy_net.jsoup_parse_title_params1;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.IOException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.InputStream;
import java.nio.charset.Charset;

public class MainActivity extends AppCompatActivity {

    EditText edt1;
    Button btnFind;
    TextView titleView;
    TextView charsetView;
    ProgressBar prBar;
    ImageView showFav;
    String S;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edt1 = (EditText) findViewById(R.id.editText1);
        btnFind = (Button) findViewById(R.id.button1);
        titleView = (TextView) findViewById(R.id.textView1);
        prBar = (ProgressBar) findViewById(R.id.progres);
        charsetView = (TextView) findViewById(R.id.textView2);
        showFav = (ImageView) findViewById(R.id.favIcon);

        View.OnClickListener onbtnFind = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TitleTask titleView = new TitleTask();
                S = edt1.getText().toString();
                titleView.execute(S);
            }
        };
        btnFind.setOnClickListener(onbtnFind);
    }

    public class TitleTask extends AsyncTask<String, Void,Bitmap> {

        String title;
        Charset chars;
        String toStrChar;
        String img;
        Bitmap bitmap;

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            prBar.setVisibility(View.VISIBLE);
            titleView.setText("Searching");
            charsetView.setText("Searching");
        }

        @Override
        protected Bitmap doInBackground(String... path) {

            try {
                Document doc = Jsoup.connect(S).get();

                title = doc.title();

                Element elimg = doc.head().select("link[href~=.*\\.(ico|png)]").first();

                img = elimg.absUrl("href");
                InputStream inputimg = new java.net.URL(img).openStream();
                bitmap= BitmapFactory.decodeStream(inputimg);

                chars = doc.charset();
                toStrChar = chars.toString();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected  void onPostExecute(Bitmap result){
            super.onPostExecute(result);
            titleView.setText(title);
            charsetView.setText(toStrChar);
            showFav.setImageBitmap(result);
            prBar.setVisibility(View.INVISIBLE);
        }
    }
}
